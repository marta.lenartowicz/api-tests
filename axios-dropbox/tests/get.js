import axios from "axios";
import { expect } from "chai";
import { token } from "../data/token.js";

const url = "https://api.dropboxapi.com/2/sharing/get_file_metadata";

describe("POST - get a file metadata", () => {
  it("get a file data", async () => {
    const headers = {
      Authorization: `Bearer ${token}`,
      "User-Agent": "api-explorer-client",
      "Content-Type": "application/json",
    };

    try {
      const response = await axios.post(
        url,
        {
          file: "/wallpapers/winter.jpg",
        },
        {
          headers: headers,
        }
      );
      expect(response.status).to.equal(200);
      expect(response.name).to.eql("winter.jpg"); // ?
      expect(response.headers["content-type"]).to.include("application/json");
      console.log("Successfully got file metadata:", response.data);
    } catch (error) {
      console.error("Error geting file data:", error.message);
    }
  });
});

// mocha tests/**/*.js
