import axios from "axios";
import { expect } from "chai";
import { token } from "../data/token.js";

const url = "https://api.dropboxapi.com/2/files/delete_v2";

describe("POST - delete a file", () => {
  it("delete a file", async () => {
    const headers = {
      Authorization: `Bearer ${token}`,
      "User-Agent": "api-explorer-client",
      "Content-Type": "application/json",
    };

    try {
      const response = await axios.post(
        url,
        {
          path: "/wallpapers/winter.jpg",
        },
        {
          headers: headers,
        }
      );
      expect(response.status).to.equal(200);
      expect(response.headers["content-type"]).to.include("application/json");
      console.log("File deleted successfully:", response.data);
    } catch (error) {
      console.error("Error deleting file:", error.message);
    }
  });
});
