import request from "supertest";
import { expect } from "chai";
import { token } from "../data/token.js";

const API_PATH = "https://api.dropboxapi.com";
const requestContainer = request(API_PATH);
const ENDPOINT = "/2/files/get_metadata";
const filePath = {
  path: "/wallpapers/sopace.jpg",
};

describe("POST - delete a file", () => {
  it("delete a file", async () => {
    const response = await requestContainer
      .post(ENDPOINT)
      .send(filePath)
      .set({ Authorization: `Bearer ${token}` })
      .set("Content-Type", "application/json");
    console.log(response.body);
    expect(response.status).to.eql(200);
    expect(response.headers["content-type"]).to.match("application/json");
  });
});
