import request from "supertest";
import { expect } from "chai";
import { token } from "../data/token.js";

const API_PATH = "https://api.dropboxapi.com";
const requestContainer = request(API_PATH);
const ENDPOINT = "/2/files/get_thumbnail";
const filePath = { path: "/wallpapers/sopace.jpg", format: { ".tag": "jpeg" } };

describe("get file thumbnail", () => {
  it("get file thumbnail", async () => {
    const response = await requestContainer
      .post(ENDPOINT)
      .send(filePath)
      .set({ Authorization: `Bearer ${token}` })
      .set("Content-Type", "application/json");
    console.log(response.body);
    expect(response.status).to.eql(200);
    expect(response.headers["content-type"]).to.match("application/json");
  });
});
